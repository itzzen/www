## Getting ahold of the webmistress

If you need to contact me, I have a few options you can use.

- [<i class="fa-brands fa-signal-messenger"></i> @itzzen.01](https://signal.me/#eu/SwvB_7c9awvAC9buFpvGSnddCq0MGZCgn6ObInB3SXGKivjY2mxLYaUt1FrpUNLN) | Signal <i class="fa-solid fa-lock">
- [<i class="fa-solid fa-envelope"></i> itzzen@tutamail.com](mailto:itzzen@tutamail.com) | E-mail <i class="fa-solid fa-key"></i>

**<i class="fa-solid fa-lock"></i> This is a secure platform.** If you need to reach out to me regarding sensitive information, these methods are preferred.

**<i class="fa-solid fa-key"></i> This profile has been verified with OpenPGP.** See my Keyoxide profile at [keyoxide.org](https://keyoxide.org/hkp/1c03da082e5712790cecbb93cb07374974d8be1e) for more information.
