## My tip jar

If you like this page, my work on my various projects, or whatever I did that has benefitted you at all, feel free to donate to me! Any amount given to me is appreciated.

### Donate using credit/debit

<a href='https://ko-fi.com/X8X810UEWR'><img height='36' style='border:0px;height:36px;width:auto;' src='https://storage.ko-fi.com/cdn/kofi1.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

### Or with crypto if that floats your boat

- <i class="fa-brands fa-monero"></i> <code>XMR: <span class="letter-break">84uzBxURYwqXk9Y66XA2V7EMpGggnwAioUkjiiwsF4G8YUiNQMRJMBbVD2tQBiweKiJfPMjG6BsUXZvnrTD95k41Uj915Tb</code>
- <i class="fa-brands fa-bitcoin"></i> <code>BTC: <span class="letter-break">bc1qp8pgl7gzxp427unv5hr69cdz5u3w4zytz2tuf5</code>

**<i class="fa-solid fa-warning"></i> Warning: Please do not add a high miner fee to your payment.** These transactions do not need to reach me instantly, so it's not worth spending a lot on fees.

**<i class="fa-solid fa-circle-info"></i> Info: Other Cryptocurrencies may be avaliable.** Please [contact me](/contact) with your preferred coin and I will send you my wallet for it.

### Referral Links

- [<i class="fa-solid fa-link"></i> Uber Referral](https://referrals.uber.com/refer?id=ej5d2sqjj4mu) | Get your first and second uber ride for 50% off (up to $10) and I will get my next two uber rides for 50% off (up to $10).
- [<i class="fa-solid fa-link"></i> Ledger Referral](https://shop.ledger.com/pages/referral-program?referral_code=1D3V43VCEYFQP) | Order a Ledger crypto wallet and I will get $20 in Bitcoin.
- ~~[<i class="fa-solid fa-link"></i> Mint Mobile Referral](http://fbuy.me/uGSLy) | Switch to Mint Mobile and you get $15 dollars in renewal credit. I will get $45 dollars in renewal credit.~~ (Referalls will open again January 1st 2026)
