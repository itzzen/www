## My presence

I have profiles on other places on the internet that may or may not contain some fun things

If you want to contact me, you should check out [my direct communication methods](/contact) instead.

### Developemnt

- [<i class="fa-solid fa-mountain"></i> itzzen](https://codeberg.org/itzzen) | Codeberg <i class="fa-solid fa-key"></i>

### Games

- [<i class="fa-brands fa-steam"></i> itzzengay](https://steamcommunity.com/id/itzzengay) | Steam
- [<i class="fa-solid fa-gamepad"></i> itzzen](https://ch.tetr.io/u/itzzen) | TETR.IO

### Music

- [<i class="fa-brands fa-bandcamp"></i> itzzen](https://bandcamp.com/itzzen) | Bandcamp
- [<i class="fa-brands fa-lastfm"></i> itzzen](https://last.fm/user/itzzen) | Last.fm

### Social

- [<i class="fa-brands fa-bluesky"></i> @itzzen.net](https://bsky.app/profile/did:plc:jzdv5iis4nn5vvtqghtwzjc2) | Bluesky <i class="fa-solid fa-key"></i>
- <i class="fa-brands fa-discord"></i> @itzzenxx | Discord <i class="fa-solid fa-key"></i>
- <i class="fa-solid fa-hashtag"></i> itzzenxx | IRC (Libera.Chat) <i class="fa-solid fa-key"></i>
- [<i class="fa-brands fa-x-twitter"></i> @itzzenxx](https://x.com/@itzzengay) | X <i class="fa-solid fa-key"></i>

### YouTube Channels

- [<i class="fa-brands fa-youtube"></i> @itzzengay](https://www.youtube.com/@itzzengay) | allissa

**<i class="fa-solid fa-key"></i> This profile has been verified with OpenPGP.** See my Keyoxide profile at [keyoxide.org](https://keyoxide.org/hkp/1c03da082e5712790cecbb93cb07374974d8be1e) for more information.
