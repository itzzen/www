## My various projects

I have created a few projects, here they are:

- [<i class="fa-solid fa-cookie-bite"></i> Cookie Patchset](https://codeberg.org/itzzen/cookie-patchset) | patches for the infamous cookie clicker idle game

## Discontinued projects

I have worked on some things in the past, here they are (sorted by discontinuation date):

- [<i class="fa-solid fa-globe"></i> Constellatory](https://constellatory.net) (Deprecated March 1st 2025) | a collection of internet serivces & the successor to Plus St
- [<i class="fa-solid fa-scroll"></i> prevent_media_downloads_from](https://codeberg.org/itzzen/prevent_media_downloads_from) (Deprecated January 3rd 2025) | a simple script for generating matrix homeserver configs to block media downloads from bad matrix homeservers
- [<i class="fa-solid fa-globe"></i> Plus St](https://plus.st) (Deprecated December 7th 2023) | a collection of internet serivces
