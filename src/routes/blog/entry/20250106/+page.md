---
title: 'the ashten posts'
date: '2025-01-06'
---

hi. I needed a place off the fediverse to house my response posts for safe keepings. If you aren't already familiar with the drama these posts are refering to, please disregard this blog post.

## Post 1:

[link to an archived version of the original post](https://archive.is/NfHmW)

Hi, allissa here. First off, thank you so fucking much to you Julia and the other instances contributing to this for finally stepping up against Ashten's power grip here, your commitment is incredible and I thank you a lot. It moves me emotionally finally seeing action being taken and makes me look at this community in a better light.

Anyways. I've noticed a lot of people share valid criticisms, discomfort & resentment, or plain rude responses in regards to this callout post. While mostly I'm filled with disgust from those types of posts, there are some that have valid worries and concern instead of just concern trolling.

I'll be responding to 2 common things I've seen floating around with this post I'm replying to.

> I'm leaving fedi if you block woem.men! / Fediblock is so stressful and is ruining fedi

I think this would be a valid criticism if Ashten wasn't a horrible person responsible for some despicable garbage. If your instance admin is a horrible person, instance admins reserve the right and will excersize that right to block and ban them from their communities. This isn't the community fracturing, this is the community healing. You, as a user, can migrate to a different instance and move away from toxic meta from drama causing system admins.

> Ashten isn't a horrible person! / This post is being over dramatic.

Yes, she is. I can account to a grooming attempt she and her ex girlfriend have tried on me. I was caught in a horrible time, with borderline abusive parents and the fallout of me getting groomed having me be at the worst mental state I've been in 2023. Ashten and Hanna swooped in, pretending to care for me and see me as a family. After Hanna got exiled for being a groomer, Ashten continued with me, going as far as to call me her "god daughter." She almost convinced me to move to the west coast in her house in Portland Oregon, moving Constellatory in the process, to live with her. Convincing me I need to get out of my house, conveniently mentioning she has housing available for me to move into. She made it very clear she would talk about how she would care for me, like a mother would. Let me mention that she was 26-27 and I was 17.

After I called Ashten out on her heinous actions towards me, she made a 6 page response post outlining the timeline in her own way. She lied numerous times in this document, and once I released more evidence to disprove her document, she reworded it to ensure she still painted herself that she was in the right. I am sure she will do the same thing with this situation, making a long document explaining nothing with no evidence to back up anything.

Anyways, I hope this post wins and people wake up to how awful Ashten truly is. I can attest to many things said here, and I hope this leads to a fedi that can heal without her. These horrible people need to be rooted out before a social network like this can be comfortable.

Thank you for reading, and sorry I'm sort of breaking my "fedi retirement" with this post. I feel the need to put in my 2 cents because I have been personally effected by these people, and I am paying close attention to how this meta plays out.

## Post 2:

[link to an archived version of the original post](https://archive.is/5ds3P)

after noticing ashten only showing specific conversations between me and her and lying about the specific incidents; I've personally decided to post the full discord conversation history onto here for the general public to interpret and come to their own conclusion on what happened.

again, sorry for saying "this will be my last post on the meta" but new things keep getting sent to me and I keep finding out about other things, so I'll be intermittent at best during this.

I hope you, the reader, will keep these in good hands. Thank you.

\- allissa

[<i class="fa-solid fa-download"></i> Direct Messages - allissa 1152061415532802120.html](https://archive.is/o/5ds3P/https://cdn.transfem.social/files/033f31f4-c76e-421a-afae-960ca39521b5)
