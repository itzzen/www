---
title: 'Updating my preferred donation method to Ko-Fi'
date: '2024-07-22'
---

tl;dr: I'm switching my credit/debit donation platform to Ko-Fi, you can donate to me via my page at [ko-fi.com/itzzen](https://ko-fi.com/itzzen).

## I really don't like Liberapay anymore

Recently I've been facing some issues involving Liberapay and the website itself. While the open source project has novel goals I agree with, the website itself is of poor quality and I can't really use it that much anymore.

My main complains are with the "weekly" revenue display on your profile. This is inaccurate and doesn't really explain your actual revenue. I can get a one-time donation for 500 dollars and it will incorrectly spread that across as weekly for an entire year, despite me getting the money in one instant.

There is also the website which is quite clunky and complicated, also the lack of basic features I think would be needed for a donations platform. For example, 2FA support is strangely absent with a message saying "Liberapay does not yet support two-factor authentication." It has been like this for years.

## Picking Ko-Fi

After seeing what some of my friends and other people I follow use, I have decided to give Ko-Fi the chance to shine. It definitely seems more geared towards what I'm looking for in a donations platform.

Many of the things I disliked about Liberapay are also not present in here. The website is fine, and TOTP 2FA is supported here, very nice to see. I hope I can report a good user experience in the next coming months.

## QnA

> I have donated to you though Liberapay in the past, what do I do?

You shouldn't need to do anything, just note that if you wish to donate to me in the future, do it through Ko-Fi. Also, you should check my website's [donations page](http://itzzen.net/donate) for any avalible methods.

> I donate weeky, monthly, etc though Liberapay, what do I do?

Cancel your payment in Liberapay or via your payment method and re-start the monthly donates on my Ko-Fi page.

## End.

Thank you for reading this, if you have any questions about this please make sure to [contact me](https://itzzen.net/contact).
