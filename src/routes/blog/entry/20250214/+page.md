---
title: "I'll be okay"
date: '2025-02-14'
---

<script>
import train from "/assets/blog/20250214/train.mp4";
</script>

hi there! it's been a long while since I posted anything meaningful to this blog, so I'm going to vent for a bit and then talk happy.

## A dreadful December

I will not go into much detail about _what_ happened, but I did not feel that great at the turn of the new year. I was stuck in the crossfire of drama and relationship issues, and I had zero clue how to properly handle all of that mess. In many ways, this ended up being a pivotal part of my life that I had not expected to go though. Consider it a "wake up call" I guess.

## Looking ahead positively

Despite everything I've experienced, and lost... I write this blog post with a smile. I feel that I'll be okay. I have restarted a lot of friendships that ended up cold and I'm trying to improve myself bit by bit. My grades have never been higher, I've joined a band with a very close friend of mine, and overall things are just looking brighter for me. I end this short vent of blog post with a positive note. Because I will get better, and I will be here to experience the rest of the new year. Thank you for those who have supported me, and given me care thoughought all of this turmoil. Love you all. Good Night.

<video controls>
	<source alt="a train speeding past the station" src="{train}" type="video/mp4">
</video>

_Recorded Feburary 3rd 2025 @ Southport, Connecticut._
