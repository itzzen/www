---
title: 'As of July 20th, I am no longer on the fediverse'
date: '2024-07-20'
---

# the title says it all really

I am now no longer on fedi. I have deleted the social.itzzen.net instances and all local content has been erased.

Felt like making this a blog post because I want to mark this as official, since I really don't want any more involvment with that platform anymore.

## QnA

> will you ever come back to fedi?

probably not.

> there's old/new drama involving you, what do I do?

ignore it and don't tell me about it as ignorance is bliss :)

> I still see @itzzenxx@\... or @itzzen@\... around fedi when searching, what about those accounts?

those are likely ghost accounts or imposters, please don't interact with them.

## end.

If you have any other questions about this, you can [contact me](https://itzzen.net/contact) and ask me directly.
