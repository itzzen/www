---
title: 'My experience taking public transit to Mohegan Sun'
date: '2024-12-09'
---

<script>
import banner from "/assets/blog/20241209/banner.png";
</script>

_in summary..._

![the mta, shore line east, southeast area transit district, and mohegan sun logos with an arrow going though one another. ]({banner})

# Before we get started...

A while ago I was on a weekend trip to the Mohegan Sun casino, and since my parents have been taking me there I've always went there by car and assumed there was no public transit route... I was wrong, _very_ wrong! My time in New York and me researching train routes along NYC and Connecticut has shown me there's more to explore here, so I decided to take a change of pace and take the train up to Mohegan Sun!

My route was divided into 2 transfers, so I will call the routes "legs" on this blog post and categorize them as they differ in some ways and will give my experience and "rating" on each one. So, let the journy begin!

# Leg 1 - Westport to New Haven

I find myself at the Westport Metro-North train station waiting for a train to go up to New Haven. I got my ticket (fter running and losing my breath after realizing the machine was on the other side of the station & the train was going to arrive soon... whoops.) and then boarded the train. The fare for this part of the trip was $6.00, and it took around 1 hour 30 minutes to arrive at the New Haven Union Station.

This was a Metro-North train operated by the MTA, which I've taken many times before, altho haven't terminated in New Haven up until this point. The train model in service was the [M8s](<https://en.wikipedia.org/wiki/M8_(railcar)>) which are all over the line, and I like them a lot. They are quite comftorable with excelete seats. Luggage racks are easy, and the windows were clear (unlike the [LIRR](https://abc7ny.com/post/lirr-problems-agency-customers-fed-sun-damaged-windows-causing-limited-visibility-during-rides/15431804/) ;) ) so I could see the scenic environment go by. The east side view of the train really is pretty, especially during the second leg of my trip.

## A quick peep in New Haven!

Because I was in the area and the Metro-North trains terminate at New Haven--and the fact I was with a friend during this side of the trip--I decided to take the walk to Frank Pepe's Pizzarea! It was a chilly Friday night and I was very hungry, so we went to have some dinner. There weren't any lines to deal with considering the time of day, so it was very easy to find a seat and quick to get our pizza. It was _very good_ and I highly reccomend you try it out someday if you've never had it if you're in the area! Anyways, me and my friend parted ways and I went further up east in my second leg of this journy.

# Leg 2 - New Haven to New London

This was the second and last train journy I would be taking this trip. Getting the ticket was very old fashioned though, having to go up to a teller to get my ticket. The fare was $10.75 one-way. Anyways, boarding the Shore Line East train to New London was no different from the Metro-North trains I've taken before due to the fact they're using the same train sets as the MTA. The only difference being the scenic environment and the ASIs. I had no complaints about the train sets, I liked them before and I'll like them again. After another 1 hour and 30 minutes, I pulled into the New London station and walked to the third and last leg of my journy.

I will mention answer a question the might be curious reader might have:

> why the need to transfer at New Haven?

Well, while I could take Amtrak which would solve the transfer, it was more expensive and less local (Westport is closer to me than Bridgeport, the closest Amtrak station). I will say it is the faster way to New London if you aren't going to be stopping at any local stops, you should probably take that instead.

Anyways, onto the last and final leg of the trip!

# Leg 3 - New London to Mohegan Sun

This was not a train, but a bus! The SEAT operates a bus on route "1" which goes from the new london train station and terminates at the norwich transportation center. Along the stops is the Mohegan Sun casino, so that was the bus for me to take.

The seats were okay, it was just a regular bus and it seemed quite basic so there isn't much to say here really. Because the bus didn't have tap to pay I had to put in some coins. The fare was $1.75. The ride itself was quite comftorable overall albeit more shakey due to it being a bus. Overall, it was good.

# Arrived at Mohegan Sun! & Conclusion

After getting dropped off at Mohegan Sun from my bus, I walked to my hotel room and settled in. I enjoyed my trip! I always like trains and I enjoyed taking one into New York City, but going the other way to New London was a new and exciting experience that I enjoyed a lot. It was very affordable, the ride was very comftorable, and my fears of driving were replaced with enjoyment! I would take it again. _And now, for everyone's burning question..._

# Should you, the reader, take this route?

If you're in Connecticut, New York City, or in that general area, I reccomend you do! It's affordable, comftorable, and very scenic! especially along the Shore Line East. If you're further away, best you take Amtrak to New London and then take the bus. Anyways, that's all I have left for today, goodbye!
