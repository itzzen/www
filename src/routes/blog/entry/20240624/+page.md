---
title: 'The big website overhaul and (the first) changelog!'
date: '2024-06-24'
---

<script>
import website from "/assets/blog/20240624/website.png";
import meta from "/assets/blog/20240624/meta.png";
import buttons from "/assets/blog/20240624/buttons.png";
</script>

## welcome to the slightly new itzzen.net

![screenshot of the revamped itzzen.net homepage]({website})

I got bored so I decided to slightly redecorate my website a bit, and I'm _super_ bored so I'm writing a blog post about it!

### so, what's new?

here are the big changes I wanna highlight

#### new "meta" page and re-arranged nav bar

![cursor hovering over the navlink named 'meta']({meta})

the home page was cluttered and I wanted a page just for web stuff that seemed to overfill the home page, so I pushed that over to a seperate page! I also moved the blog (formerly writeups) page up to have higher priority

also, any external link now displays an "<i class="shrink fa-solid fa-arrow-up-right-from-square"></i>" icon next to it

#### writeups are now just blogs

gosh, I felt so special naming the blogs something that wasn't "blog." But the special name seems quite cheesy so I just named it to what it is. A _blog_ page.

#### 88x31 galore!

![a bunch of 88x31 website buttons]({buttons})

regular adding and updating of my 88x31 button stash. The majority of my curated collection has been moved to the new meta page with only 3 buttons on the main page. Those being an interactive one, my website's button, and my girlfriend's button.

### ok, but, what's _not_ new?

#### the about me page is still not done!

I keep not doing it! and it won't get done until I can figure out what to put on there! also writing about me pages in general is hard so this will be a while :(

## it's a wrap!

thank you for listening to my Ted talk, or something. haha. I can't really figure out how to end these so cya :3
